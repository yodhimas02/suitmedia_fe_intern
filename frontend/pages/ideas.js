import dynamic from 'next/dynamic';
import { Suspense } from 'react';
import ErrorBoundary from '../components/ErrorBoundary';
import PostList from '../components/PostList';

const Banner = dynamic(() => import('../components/Banner'), {
  suspense: true,
});

export default function Ideas() {
  return (
    <ErrorBoundary>
      <Suspense fallback={<div className="w-full h-screen flex items-center justify-center"><div className="spinner"></div></div>}>
        <Banner />
      </Suspense>
      <div className="container mx-auto py-8">
        <PostList />
      </div>
    </ErrorBoundary>
  );
}
