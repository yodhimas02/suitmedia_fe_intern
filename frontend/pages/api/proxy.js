import { createProxyMiddleware } from 'http-proxy-middleware';

const proxy = createProxyMiddleware({
  target: 'https://assets.suitdev.com',
  changeOrigin: true,
  pathRewrite: {
    '^/api/proxy/': '/',
  },
});

export default function handler(req, res) {
  return proxy(req, res);
}
