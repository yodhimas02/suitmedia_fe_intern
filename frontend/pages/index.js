import dynamic from 'next/dynamic';
import ErrorBoundary from '../components/ErrorBoundary';

const Banner = dynamic(() => import('../components/Banner'), {
  suspense: true,
});

export default function Work() {
  return (
    <ErrorBoundary>
    </ErrorBoundary>
  );
}
