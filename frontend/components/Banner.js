import { Suspense, useEffect, useRef, useState } from 'react';

const Banner = () => {
  const [bannerData, setBannerData] = useState(null);
  const [loading, setLoading] = useState(true);
  const bannerRef = useRef(null);
  const textRef = useRef(null);

  useEffect(() => {
    const fetchBanner = async () => {
      try {
        const response = await fetch('http://localhost:1337/api/banners?populate=*');
        if (!response.ok) {
          throw new Error('Failed to fetch banner data');
        }
        const data = await response.json();
        setBannerData(data.data[0].attributes);
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false);
      }
    };
    fetchBanner();

    const handleScroll = () => {
      const scrollTop = window.pageYOffset;
      if (bannerRef.current) {
        bannerRef.current.style.transform = `translateY(${scrollTop * 0.5}px)`;
      }
      if (textRef.current) {
        textRef.current.style.transform = `translateY(${scrollTop * 0.3}px)`;
      }
    };

    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  if (loading) {
    return (
      <div className="w-full h-screen flex items-center justify-center">
        <div className="spinner"></div>
      </div>
    );
  }

  if (!bannerData) return null;

  const imageUrl = bannerData.image?.data?.attributes?.url
    ? `http://localhost:1337${bannerData.image.data.attributes.url}`
    : '/path/to/default/image.jpg';

  return (
    <section className="relative w-full h-screen overflow-hidden">
      <div
        ref={bannerRef}
        className="absolute top-0 left-0 w-full h-full bg-cover bg-center opacity-75"
        style={{ backgroundImage: `url(${imageUrl})` }}
      ></div>
      <div className="absolute bottom-0 left-0 w-full h-16 bg-white transform origin-bottom-right clip-path-triangle"></div>
      <div
        ref={textRef}
        className="relative z-10 flex flex-col items-center justify-center w-full h-full text-center"
      >
        <h1 className="text-5xl font-bold text-white">{bannerData.title}</h1>
        <p className="mt-4 text-lg text-white">{bannerData.description}</p>
      </div>
    </section>
  );
};

export default Banner;
