import React, { useState, useEffect } from 'react';
import Image from 'next/image';

const PostList = () => {
  const [posts, setPosts] = useState([]);
  const [sortOrder, setSortOrder] = useState('newest');
  const [itemsPerPage, setItemsPerPage] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [totalItems, setTotalItems] = useState(0);

  useEffect(() => {
    const fetchPosts = async () => {
      const sortParam = sortOrder === 'newest' ? '-published_at' : 'published_at';
      const apiUrl = `https://suitmedia-backend.suitdev.com/api/ideas?page[number]=${currentPage}&page[size]=${itemsPerPage}&append[]=small_image&append[]=medium_image&sort=${sortParam}`;
      try {
        const response = await fetch(apiUrl, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
        });

        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }

        const data = await response.json();
        console.log('Fetched data:', data);

        const pageCount = data.meta?.last_page || 1;
        const totalCount = data.meta?.total || 0;

        setPosts(data.data || []);
        setTotalPages(pageCount);
        setTotalItems(totalCount);
      } catch (error) {
        console.error('Failed to fetch posts:', error);
        setPosts([]);
      }
    };

    fetchPosts();
  }, [sortOrder, itemsPerPage, currentPage]);

  useEffect(() => {
    const savedSortOrder = localStorage.getItem('sortOrder');
    const savedItemsPerPage = localStorage.getItem('itemsPerPage');
    const savedCurrentPage = localStorage.getItem('currentPage');

    if (savedSortOrder) setSortOrder(savedSortOrder);
    if (savedItemsPerPage) setItemsPerPage(Number(savedItemsPerPage));
    if (savedCurrentPage) setCurrentPage(Number(savedCurrentPage));
  }, []);

  useEffect(() => {
    localStorage.setItem('sortOrder', sortOrder);
    localStorage.setItem('itemsPerPage', itemsPerPage);
    localStorage.setItem('currentPage', currentPage);
  }, [sortOrder, itemsPerPage, currentPage]);

  const handleSortChange = (e) => {
    setSortOrder(e.target.value);
    setCurrentPage(1); // Reset to first page on sort change
  };

  const handleItemsPerPageChange = (e) => {
    setItemsPerPage(Number(e.target.value));
    setCurrentPage(1); // Reset to first page on items per page change
  };

  const handlePageChange = (page) => {
    if (page >= 1 && page <= totalPages) {
      setCurrentPage(page);
    }
  };

  const renderPageNumbers = () => {
    const pageNumbers = [];
    const maxPagesToShow = 5;
    let startPage = Math.max(1, currentPage - Math.floor(maxPagesToShow / 2));
    let endPage = Math.min(totalPages, startPage + maxPagesToShow - 1);

    if (endPage - startPage < maxPagesToShow - 1) {
      startPage = Math.max(1, endPage - maxPagesToShow + 1);
    }

    for (let i = startPage; i <= endPage; i++) {
      pageNumbers.push(
        <li key={i} className={`mx-1 px-3 py-2 ${currentPage === i ? 'bg-orange-500 text-white' : 'bg-gray-200'}`}>
          <button onClick={() => handlePageChange(i)}>{i}</button>
        </li>
      );
    }

    return pageNumbers;
  };

  return (
    <div className="container mx-auto">
      <div className="flex justify-between items-center my-4">
        <div className="text-gray-700">
          Showing {((currentPage - 1) * itemsPerPage) + 1} - {Math.min(currentPage * itemsPerPage, totalItems)} of {totalItems}
        </div>
        <div className="flex items-center space-x-4">
          <div className="border rounded-lg px-3 py-1">
            <span>Show per page: </span>
            <select value={itemsPerPage} onChange={handleItemsPerPageChange}>
              <option value={10}>10</option>
              <option value={20}>20</option>
              <option value={50}>50</option>
            </select>
          </div>
          <div className="border rounded-lg px-3 py-1">
            <span>Sort by: </span>
            <select value={sortOrder} onChange={handleSortChange}>
              <option value="newest">Newest</option>
              <option value="oldest">Oldest</option>
            </select>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
        {posts.map((post) => {
          const imageUrl = post.medium_image?.[0]?.url || '/default-image.jpg';
          return (
            <div key={post.id} className="border rounded-lg overflow-hidden shadow-lg h-80 flex flex-col">
              <Image
                src={imageUrl}
                alt={post.title || 'Default image'}
                width={300}
                height={200}
                className="w-full h-48 object-cover"
                onError={(e) => { e.target.src = '/default-image.jpg'; }}
              />
              <div className="p-4 flex-grow">
                <p className="text-gray-500">{new Date(post.published_at).toLocaleDateString()}</p>
                <h2 className="text-xl font-bold h-20 overflow-hidden overflow-ellipsis">
                  {post.title || 'Default Title'}
                </h2>
              </div>
            </div>
          );
        })}
      </div>
      <div className="flex justify-center mt-4">
        <nav>
          <ul className="flex list-none">
            <li className={`mx-1 px-3 py-2 ${currentPage === 1 ? 'bg-gray-200' : 'hover:bg-orange-500 text-black'}`}>
              <button onClick={() => handlePageChange(1)} disabled={currentPage === 1}>
                &laquo;
              </button>
            </li>
            <li className={`mx-1 px-3 py-2 ${currentPage === 1 ? 'bg-gray-200' : 'hover:bg-orange-500 text-black'}`}>
              <button onClick={() => handlePageChange(currentPage > 1 ? currentPage - 1 : 1)} disabled={currentPage === 1}>
                &lt;
              </button>
            </li>
            {renderPageNumbers()}
            <li className={`mx-1 px-3 py-2 ${currentPage === totalPages ? 'bg-gray-200' : 'hover:bg-orange-500 text-black'}`}>
              <button onClick={() => handlePageChange(currentPage < totalPages ? currentPage + 1 : totalPages)} disabled={currentPage === totalPages}>
                &gt;
              </button>
            </li>
            <li className={`mx-1 px-3 py-2 ${currentPage === totalPages ? 'bg-gray-200' : 'hover:bg-orange-500 text-black'}`}>
              <button onClick={() => handlePageChange(totalPages)} disabled={currentPage === totalPages}>
                &raquo;
              </button>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default PostList;
