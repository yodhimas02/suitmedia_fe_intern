import { useState, useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useScrollDirection } from '../hooks/useScrollDirection';

const Header = () => {
  const router = useRouter();
  const scrollDirection = useScrollDirection();
  const [isVisible, setIsVisible] = useState(true);

  useEffect(() => {
    if (scrollDirection === 'down') {
      setIsVisible(false);
    } else if (scrollDirection === 'up') {
      setIsVisible(true);
    }
  }, [scrollDirection]);

  const navItems = [
    { name: 'Work', path: '/' },
    { name: 'About', path: '/about' },
    { name: 'Services', path: '/services' },
    { name: 'Ideas', path: '/ideas' },
    { name: 'Careers', path: '/careers' },
    { name: 'Contact', path: '/contact' },
  ];

  return (
    <header className={`fixed top-0 w-full z-50 transition-transform duration-300 ${!isVisible ? '-translate-y-full' : 'translate-y-0'} bg-orange-600 bg-opacity-90`}>
      <div className="container mx-auto flex justify-between items-center p-4">
        <div className="text-white">
          <img src="/suitmedia-logo.png" alt="Logo" className="h-16 w-36"/>
        </div>
        <nav className="flex space-x-8">
          {navItems.map((item) => (
            <Link href={item.path} key={item.name} legacyBehavior>
              <a className={`nav-item ${router.pathname === item.path ? 'nav-item-active' : ''}`}>
                {item.name}
              </a>
            </Link>
          ))}
        </nav>
      </div>
    </header>
  );
};

export default Header;
